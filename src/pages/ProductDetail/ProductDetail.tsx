import React from 'react';
import {useParams} from "react-router-dom";
import {useGetProductById} from "../../api/product";
import styled from "styled-components";
import {Button, Col, Image, Row, Skeleton} from "antd";
import {IProduct, useCartStore} from "../../store/cart";

const ProductDetail = () => {
    const params: any = useParams();
    const {checkItemInCart, addProduct, incrementProduct} = useCartStore();
    const {data: ProductDetail, isLoading} = useGetProductById(params.id);
    const addToCart = () => {
        addProduct({
            id: params.id,
            name: ProductDetail?.name as string,
            price: ProductDetail?.price as number,
            image: ProductDetail?.image as string
        })
    }
    return (
        <Container>
            <Row className="content">
                <Col span={5} className="product-image">
                    <Skeleton loading={isLoading}>
                        <Image src={ProductDetail?.image as string} loading="eager" preview={false}/>
                    </Skeleton>
                </Col>
                <Col span={13}>
                    <div className="product-detail">
                        <div className="product-category">
                            Category: {ProductDetail?.category.name}
                        </div>
                        <div className="product-category">
                            {ProductDetail?.name}
                        </div>
                        <div className="product-description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum turpis sed ipsum
                            sagittis scelerisque. Nulla condimentum, dolor at lobortis vulputate, metus massa
                            laoreet
                            justo, id suscipit velit justo faucibus ex. Curabitur maximus, quam sit amet convallis
                            cursus, dui nibh consectetur velit, sed pharetra sapien dui eu orci. In consequat at mi
                            scelerisque ullamcorper. Suspendisse lobortis, elit sed mollis hendrerit, libero ipsum
                            rutrum nisi, id sodales nibh mi non tellus. Aenean a augue nunc. Donec eget fringilla
                            tellus.
                        </div>
                        <div className="product-data">
                            {/*<div className="product-inventory">Inventory: {ProductDetail?.inventory}</div>*/}
                            <div className="product-price">
                                ${ProductDetail?.price}
                            </div>
                            <div className="product-action">
                                {
                                    !checkItemInCart(params.id) ? (
                                        <Button type="primary" size="large"
                                                onClick={addToCart}>Add To
                                            Cart</Button>
                                    ) : (
                                        <Button type="primary" size="large" onClick={() => incrementProduct(params.id)}>Add
                                            More</Button>
                                    )
                                }

                            </div>
                        </div>

                    </div>
                </Col>
            </Row>
        </Container>
    );
};

export default ProductDetail;
const Container = styled.div`
  .content {
    border: 1px solid #e1e1e1;
  }

  .product-image {
    .ant-image {
      width: 100%;
      height: 300px;

      img {
        width: 100%;
        height: 100%;
        object-fit: cover;
      }
    }
  }

  .product-detail {
    padding: 0 20px;
    height: 100%;
    display: flex;
    flex-direction: column;

    .product-category {
      font-weight: bold;
      font-size: 20px;
      margin-bottom: 20px;
      margin-top: 10px;
      color: #818181;;
    }

    .product-description {
      font-size: 14px;
      color: #4a4a4a
    }

    .product-data {
      margin-bottom: 20px;
      //margin-bottom: auto;
      flex: 1 1 auto;
      display: flex;
      flex-direction: column;
      justify-content: flex-end;

      .product-inventory {
      }

      .product-price, .product-inventory {
        font-weight: bold;
        font-size: 18px;
        margin-bottom: 10px;
      }

      .product-action {

      }
    }

  }

`