import React from 'react';
import styled from "styled-components";
import {Link} from "react-router-dom";
import {Button} from "antd";
import {ArrowRightOutlined} from "@ant-design/icons";
import banner from '../../assets/banner.jpg';

const Banner = () => {
    return (
        <Container>
            <div className="banner-desc">
                <h1 className="text-thin" style={{fontSize: "30px"}}>
                    <strong>Buy</strong>&nbsp;everything with&nbsp;<strong>MStore</strong>
                </h1>
                <p style={{fontWeight: 500, fontSize: "16px"}}>Buying eyewear should leave you happy and good-looking, with money in
                    your pocket. Glasses, sunglasses, and contacts—we’ve got your eyes covered.
                </p>
                <Link to="/shop">
                    <Button size="large" type="primary" icon={<ArrowRightOutlined/>}>
                        Shop Now
                    </Button>
                </Link>
            </div>
            <div className="banner-img">
                <img src={banner} alt="banner"/>
            </div>
        </Container>
    );
};

export default Banner;
const Container = styled.div`
  background: white;
  display: flex;
  width: 100%;
  height: 400px;
  .banner-desc {
    flex-basis: 50%;
    padding: 40px;
  }

  .banner-img {
    flex-basis: 50%;

    img {
      width: 100%;
      height: 100%;
      object-fit: cover;
    }
  }
`