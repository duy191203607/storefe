import React from 'react';
import Banner from "./Banner";
import styled from "styled-components";

const Home = () => {
    return (
        <Container>
            <Banner/>
        </Container>
    );
};

export default Home;
const Container = styled.div`
  min-height: calc(100vh - var(--headerHeight) - var(--footerHeight));
  padding: 12px 0;
`