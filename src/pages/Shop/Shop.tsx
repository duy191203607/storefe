import React from 'react';
import styled from "styled-components";
import {Col, Dropdown, Menu, Pagination, Row} from "antd";
import {useGetProduct} from "../../api/product";
import {useGetCategory} from "../../api/category";
import {DownOutlined} from "@ant-design/icons";
import {useShopProductPagination} from "../../store/product";
import ProductItem from "../../components/common/ProductItem";
import Loading from '../../components/common/Loading';
import {emptyProductUrl} from '../../constants';

enum FilterType {
    "CATEGORY" = "category",
    "SORT" = "sort"
}


const Shop = () => {
    const {data: Product, isLoading: ProductLoading} = useGetProduct();
    const {data: Category} = useGetCategory();
    const {
        page,
        updateCategory,
        updateSort,
        updatePaginationPage,
        resetPagination
    } = useShopProductPagination();
    const onFilterSelect = ({key}: any, type: any) => {
        switch (type) {
            case FilterType.SORT: {
                updateSort(key);
                resetPagination()
                break;
            }
            case FilterType.CATEGORY: {
                updateCategory(key);
                resetPagination()
            }
        }
    }
    const filterMenu = (
        <Menu onClick={(v) => onFilterSelect(v, FilterType.SORT)}>
            <Menu.Item key={null}>Default</Menu.Item>
            <Menu.Item key="ASC">Price: Low to High</Menu.Item>
            <Menu.Item key="DESC">Price: High to Low</Menu.Item>
        </Menu>
    )
    const onPaginationChange = (page: number, pageSize: number) => {
        updatePaginationPage(page, pageSize);
        // resetPagination();
    }
    return (
        <Container>
            <Row gutter={16}>
                <Col span={4}>
                    <div className="category-filter-temp"/>
                    <Menu theme="light" mode="vertical" defaultSelectedKeys={["null"]}
                          onClick={(v) => onFilterSelect(v, FilterType.CATEGORY)}>
                        <Menu.Item key={"null"}>All</Menu.Item>
                        {
                            Category && Category.map((v) => {
                                return (
                                    <Menu.Item key={v.id}>{v.name}</Menu.Item>
                                )
                            })
                        }
                    </Menu>
                </Col>
                <Col span={20}>
                    <div className="filter-button">
                        <Dropdown overlay={filterMenu} trigger={['click']}>
                            <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                                Product filter <DownOutlined/>
                            </a>
                        </Dropdown>
                    </div>
                    <div className="product-container">
                        <Row gutter={[14, 14]}>
                            {
                                <>
                                    {
                                        Product === undefined ? <Loading size={100}/> : (
                                            <>
                                                {
                                                    Product && Product.result.map((v: any, i: any) => {
                                                        return (
                                                            <Col span={4} key={v.id}>
                                                                <ProductItem id={v.id} name={v.name} price={v.price}
                                                                             image={v.image ?? emptyProductUrl}
                                                                             isLoading={ProductLoading}/>
                                                            </Col>
                                                        )
                                                    })
                                                }
                                            </>
                                        )
                                    }
                                </>
                            }
                        </Row>
                    </div>
                    <div className="pagination-container">
                        <Pagination defaultCurrent={1} total={Product?.total} current={page} hideOnSinglePage={false}
                                    pageSizeOptions={[10, 20, 30]}
                                    onChange={(page, pageSize) => onPaginationChange(page, pageSize)}/>
                    </div>
                </Col>
            </Row>
        </Container>
    );
};

export default Shop;
const Container = styled.div`
  max-height: 100%;
  overflow-y: auto;
  overflow-x: hidden;
  min-height: calc(100vh - var(--headerHeight) - var(--footerHeight));
  padding: 12px 0;

  .category-container {
    list-style-type: none;
    font-family: 'Tajawal', Helvetica, Arial, sans-serif;
    font-weight: bold;
  }

  .ant-menu-item {
    margin: 0;
    font-weight: bold;
  }

  .category-filter {
    margin-top: 30px;
  }

  .category-filter-temp {
    height: 60px;
  }

  .filter-button {
    margin-top: 5px;
    display: flex;
    justify-content: flex-end;

    .ant-dropdown-link {
      margin-right: 0;
      border: 1px solid var(--primaryColor);
      padding: 5px 8px;
    }
  }

  .product-container {
    //max-height: 650px;
    margin-top: 20px;
    //max-height: 100vh;

    //overflow-y: auto;
    //overflow-x: hidden;

  }

  .pagination-container {
    margin-top: 20px;
    display: flex;
    justify-content: flex-end;
  }
`