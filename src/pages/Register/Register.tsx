import React from 'react';
import {Button, message, Typography} from "antd";
import styled from "styled-components";
import * as Yup from 'yup';
import {Formik, Form} from 'formik';
import InputField from "../../components/common/InputField";
import {Link, useNavigate} from "react-router-dom";
import {useRegister} from "../../api/auth";
import {useAuthStore} from "../../store/auth";

const SignupSchema = Yup.object().shape({
    name: Yup.string(),
    email: Yup.string().email('Invalid email').required('This field is require'),
    password: Yup.string().min(6, "Min length is 6").required('This field is require'),
    repeatPassword: Yup.string().required('This field is require').oneOf([Yup.ref("password"), null], "Confirm password do not match!"),
    phone: Yup.string().matches(/^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$/, "Phone number is not valid"),
    address: Yup.string().optional(),
});
const {Title} = Typography;
const Register = () => {
    const {updateAuthState} = useAuthStore();
    const _Register = useRegister();
    const navigate = useNavigate();
    const onSubmit = async (values: any) => {
        await _Register.mutateAsync(
            {
                email: values.email,
                name: values.name,
                password: values.password,
                phone: values.phone,
                address: values.address,
            }, {
                onSuccess: (res) => {
                    const {data: {email, id, name}} = res;
                    updateAuthState({isAuth: true, email, userID: id, name});
                    message.success("Register successfully", 3);
                    navigate("/");
                },
                onError: (error: any) => {
                    message.error(error.response.data.error, 3)
                }
            })
    }
    return (
        <Container>
            <Title level={3} className="title">Register</Title>
            <Formik
                initialValues={{
                    name: '',
                    password: '',
                    repeatPassword: '',
                    email: '',
                    phone: '',
                    address: ''
                }}
                validationSchema={SignupSchema}
                onSubmit={onSubmit}
            >
                {({values, errors, touched}) => (
                    <Form autoComplete="off">
                        <InputField errors={errors} touched={touched} values={values} name="name" placeholder="Name"/>
                        <InputField errors={errors} touched={touched} values={values} name="email" placeholder="Email"/>
                        <InputField errors={errors} touched={touched} values={values} name="password" type="password"
                                    placeholder="Password"/>
                        <InputField errors={errors} touched={touched} values={values} name="repeatPassword" type="password"
                                    placeholder="Repeat Password"/>
                        <InputField errors={errors} touched={touched} values={values} name="phone" type="text"
                                    placeholder="Phone number"/>
                        <InputField errors={errors} touched={touched} values={values} name="address" type="text"
                                    placeholder="Address"/>
                        <div>
                            <Link to="/auth/login">
                                Have an account? Login now
                            </Link>
                        </div>
                        <div className="submit-button" style={{marginTop: "10px"}}>
                            <Button htmlType="submit" type="primary" style={{width: "100%"}}>Register</Button>
                        </div>
                    </Form>
                )}
            </Formik>
        </Container>
    );
};

export default Register;
const Container = styled.div`
  width: 400px;
  margin: auto;
  height: calc(100vh - var(--headerHeight) - var(--footerHeight));
  display: flex;
  flex-direction: column;
  justify-content: center;

  .title {
    text-align: center;
  }
`