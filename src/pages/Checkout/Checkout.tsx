import {Button, Steps} from 'antd';
import React from 'react';
import styled from "styled-components";
import ConfirmCartItem from "./ConfirmCartItem";
import AddressShipping from "./AddressShipping";
import {useCartStore} from "../../store/cart";

const {Step} = Steps;
const Checkout = () => {
    const {cartSubmitting} = useCartStore();
    const [current, setCurrent] = React.useState(0);
    const next = () => {
        setCurrent(current + 1);
    };
    const prev = () => {
        setCurrent(current - 1);
    };
    const steps = [
        {
            title: 'Order Summary',
            content: <ConfirmCartItem/>,
        },
        {
            title: 'Shipping Details',
            content: <AddressShipping/>,
        }
    ];
    return (
        <Container>
            <Steps current={current}>
                {steps.map(item => (
                    <Step key={item.title} title={item.title}/>
                ))}
            </Steps>
            <div className="steps-content">{steps[current].content}</div>
            <div className="steps-action">
                {current < steps.length - 1 && (
                    <Button type="primary" onClick={() => next()}>
                        Next
                    </Button>
                )}
                {current === steps.length - 1 && (
                    <Button type="primary" htmlType="submit" form="address-shipping-form" loading={cartSubmitting}>
                        Done
                    </Button>
                )}
                {current > 0 && (
                    <Button style={{margin: '0 8px'}} onClick={() => prev()}>
                        Previous
                    </Button>
                )}
            </div>
        </Container>
    );
};

export default Checkout;
const Container = styled.div`
  padding: 12px 0;
  min-height: calc(100vh - var(--headerHeight) - var(--footerHeight));
  width: 70%;
  margin: auto;
`