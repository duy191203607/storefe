import React from 'react';
import * as Yup from "yup";
import styled from "styled-components";
import {Form, Formik} from "formik";
import InputField from "../../components/common/InputField";
import {message, Typography} from "antd";
import {useCreateNewOrder} from "../../api/order";
import {useAuthStore} from "../../store/auth";
import {useCartStore} from "../../store/cart";
import {useNavigate} from "react-router-dom";

const {Title} = Typography;
const AddressShipping = () => {
    const navigation = useNavigate();
    const _createOrder = useCreateNewOrder();
    const {userID} = useAuthStore();
    const {products, totalPrice, clearCart, toggleCartSubmit} = useCartStore();
    const AddressShippingSchema = Yup.object({
        name: Yup.string().required('This field is require').max(200, "Max length is 200"),
        email: Yup.string().email('Invalid email').optional(),
        phone: Yup.string().required('This field is require').matches(/^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$/, "Phone number is not valid"),
        address: Yup.string().required('This field is require'),
    })
    const onSubmit = async (values: any) => {
        toggleCartSubmit();
        await _createOrder.mutateAsync({
            accountId: userID as number,
            address: values.address,
            email: values.email,
            name: values.name,
            phone: values.phone,
            price: totalPrice,
            product: products.map(v => {
                return {
                    id: v.id,
                    quantity: v.quantity
                }
            }),
        }, {
            onSuccess: (res: any) => {
                message.success(res.message, 3);
                toggleCartSubmit();
                clearCart();
                navigation("/", {replace: true})
            },
            onError: (error: any) => {
                toggleCartSubmit();
                message.error(error.response.data.error, 3)
            }
        })
    }
    return (
        <Container>
            <Title level={3} className="title">Shipping Info</Title>
            <Formik
                initialValues={{
                    name: '',
                    email: '',
                    phone: '',
                    address: ''
                }}
                validationSchema={AddressShippingSchema}
                onSubmit={(values) => onSubmit(values)}
            >
                {({values, errors, touched}) => {
                    return (
                        (
                            <Form autoComplete="off" id="address-shipping-form">
                                <InputField errors={errors} touched={touched} values={values} name="name"
                                            placeholder="Name"/>
                                <InputField errors={errors} touched={touched} values={values} name="email"
                                            placeholder="Email"/>
                                <InputField errors={errors} touched={touched} values={values} name="phone" type="text"
                                            placeholder="Phone number"/>
                                <InputField errors={errors} touched={touched} values={values} name="address" type="text"
                                            placeholder="Address"/>
                            </Form>
                        )
                    )
                }}
            </Formik>
        </Container>
    );
}

export default AddressShipping;
const Container = styled.div`
  width: 400px;
  margin: 30px auto;
  //min-height: calc(100vh - var(--headerHeight) - var(--footerHeight));
  .title {
    text-align: center;
  }
`