import React from 'react';
import styled from "styled-components";
import {useCartStore} from "../../store/cart";
import CartItem from "../../components/CartItem";

const ConfirmCartItem = () => {
    const {products, totalPrice} = useCartStore();
    return (
        <Container>
            <div className="title-desc" style={{textAlign: "center", marginBottom: "20px", fontWeight: "bold"}}>
                Review items in your basket
            </div>
            <div className="card-item">
                {
                    products.length >= 1 ? products.map((v, i) => (
                        <CartItem key={i} id={v.id} name={v.name} price={v.price} image={v.image}
                                  quantity={v.quantity}/>
                    )) : <div>Empty</div>
                }
            </div>
            <div className="summary">
                Total price: ${totalPrice}
            </div>
        </Container>
    );
};

export default ConfirmCartItem;
const Container = styled.div`
  width: 600px;
  margin: 30px auto auto auto;
  padding: 20px;
  border: 1px dashed var(--primaryColor);
  overflow-y: auto;
  border-radius: 10px;
  //max-height: calc(100vh - var(--headerHeight) - var(--footerHeight));
  height: 650px;
  .summary {
    text-align: right;
    font-weight: bold;
    font-size: 16px;
  }

  .title-desc {
    font-size: 18px;
  }

  .card-item {
    //max-height: calc(100vh - var(--headerHeight) - var(--footerHeight) - 100px);
    overflow-y: auto;
  }
`