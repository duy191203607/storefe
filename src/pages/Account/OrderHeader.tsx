import React from 'react';
import styled from "styled-components";
import dayjs from 'dayjs'

interface Props {
    id: number,
    createdAt: string,
    price: number,
    address: string
}

const OrderHeader = ({createdAt, price, address}: Props) => {
    return (
        <Container>
            <div className="created-time">Created at: {dayjs(createdAt).format("DD-MM-YYYY")}</div>
            <div className="price">Total price: ${price}</div>
            <div className="address">Address: {address}</div>
        </Container>
    );
};

export default OrderHeader;
const Container = styled.div`
  .created-time {
    font-size: 16px;
    font-weight: 500;
  }
  .price{
    color: var(--secondaryColor)
  }
`