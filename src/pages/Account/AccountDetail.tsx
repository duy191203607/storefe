import React, {useState} from 'react';
import styled from "styled-components";
import {
    testCurrentPassword,
    useGetAccountDetail,
    useUpdateAccountInfo,
    useUpdateAccountPassword
} from "../../api/account";
import {Button, message, Skeleton} from "antd";
import {Form, Formik} from "formik";
import InputField from "../../components/common/InputField";
import * as Yup from "yup";
import {useAuthStore} from "../../store/auth";
import _ from "lodash";
import {useQueryClient} from "react-query";

const AccountDetail = () => {
    const {userID, logout, email, updateAuthState} = useAuthStore();
    const queryClient = useQueryClient();
    const _UpdateInfo = useUpdateAccountInfo();
    const _UpdatePassword = useUpdateAccountPassword();
    const AccountEditSchema = Yup.object().shape({
        name: Yup.string(),
        email: Yup.string().email('Invalid email').required('This field is require'),
        phone: Yup.string().matches(/^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$/, "Phone number is not valid"),
        address: Yup.string().optional(),
    });
    const PasswordEditSchema = Yup.object().shape({
        currentPassword: Yup.string().test("Current password", "Current password do not match", _.debounce<any>((value: any) => {
                if (value === undefined) {
                    return false
                } else {
                    return testCurrentPassword(value as string, userID as number).then(res => {
                        return res;
                    }).catch(e => {
                        if (e.response.status === 400) return false;
                    });
                }
            }, 1500)
        ).required('This field is require'),
        newPassword: Yup.string().min(6, "Min length is 6").required('This field is require'),
        repeatPassword: Yup.string().required('This field is require').oneOf([Yup.ref("newPassword"), null], "Confirm password do not match!"),
    })
    const {data, isLoading} = useGetAccountDetail();
    const [accountEdit, setAccountEdit] = useState(false);
    const [passwordEdit, setPasswordEdit] = useState(false);
    let body;
    const onSubmitAccountInfo = async (values: any) => {
        await _UpdateInfo.mutateAsync({
            email: (values.email !== email ? values.email : undefined),
            name: values.name,
            phone: values.phone,
            address: values.address
        }, {
            onSuccess: async (res: any) => {
                const {id, name, email} = res;
                updateAuthState({userID: id, name, email, isAuth: true});
                setPasswordEdit(false);
                setAccountEdit(false);
                message.success("Update account info success", 3);
                await queryClient.invalidateQueries("account/detail")
            },
            onError: (error: any) => {
                message.error(error.response.data.error, 3)
            }
        })
    }
    const onSubmitNewPassword = async (values: any) => {
        await _UpdatePassword.mutateAsync({
            newPassword: values.newPassword
        }, {
            onSuccess: async () => {
                setPasswordEdit(false);
                setAccountEdit(false);
                logout();
                message.success("Update account password success", 3);
                await queryClient.invalidateQueries("account/detail")
            },
            onError: (error: any) => {
                message.error(error.response.data.error, 3)
            }
        })
    }
    if (accountEdit) {
        body = (
            <div className="form-account-update">
                <Formik
                    initialValues={{
                        name: data?.profile.name ?? "",
                        email: data?.email,
                        phone: data?.profile.phone ?? "",
                        address: data?.profile.address ?? ""
                    }}
                    validationSchema={AccountEditSchema}
                    onSubmit={onSubmitAccountInfo}
                >
                    {({values, errors, touched}) => (
                        <Form autoComplete="off">
                            <InputField errors={errors} touched={touched} values={values} name="name"
                                        placeholder="Name"/>
                            <InputField errors={errors} touched={touched} values={values} name="email"
                                        placeholder="Email"/>
                            <InputField errors={errors} touched={touched} values={values} name="phone"
                                        type="text"
                                        placeholder="Phone number"/>
                            <InputField errors={errors} touched={touched} values={values} name="address"
                                        type="text"
                                        placeholder="Address"/>
                            <div className="submit-button" style={{marginTop: "10px"}}>
                                <Button htmlType="submit" type="primary" style={{marginRight: "14px"}} loading={_UpdateInfo.isLoading}>
                                    Update profile</Button>
                                <Button type="primary" onClick={() => setAccountEdit(false)}>Cancel</Button>
                            </div>
                        </Form>
                    )}
                </Formik>
            </div>
        )
    } else if (passwordEdit) {
        body = (
            <div className="form-password-edit">
                <Formik
                    initialValues={{
                        currentPassword: "",
                        newPassword: "",
                        repeatPassword: "",
                    }}
                    validationSchema={PasswordEditSchema}
                    onSubmit={onSubmitNewPassword}
                >
                    {({values, errors, touched}) => (
                        <Form autoComplete="off">
                            <InputField errors={errors} touched={touched} values={values} name="currentPassword"
                                        type="password"
                                        placeholder="Current Password"/>
                            <InputField errors={errors} touched={touched} values={values} name="newPassword"
                                        type="password"
                                        placeholder="New Password"/>
                            <InputField errors={errors} touched={touched} values={values}
                                        name="repeatPassword" type="password"
                                        placeholder="Repeat Password"/>
                            <div className="submit-button" style={{marginTop: "10px"}}>
                                <Button htmlType="submit" type="primary" style={{marginRight: "14px"}} loading={_UpdatePassword.isLoading}>
                                    Update password</Button>
                                <Button type="primary" onClick={() => setPasswordEdit(false)}>Cancel</Button>
                            </div>
                        </Form>
                    )}
                </Formik>
            </div>
        )
    } else {
        body = (
            <>
                <h1>My profile</h1>
                <p style={{fontSize: "16px", marginBottom: "20px"}}>
                    Manage profile information for account security
                </p>
                <h3>{data?.profile.name}</h3>
                <h4 className="account-info">Email</h4>
                <p>{data?.email}</p>
                <h4 className="account-info">Address</h4>
                <p>{data?.profile.address ?? "Address not set"}</p>
                <h4 className="account-info">Mobile</h4>
                <p>{data?.profile.phone ?? "Phone number not set"}</p>
                <Button
                    type="primary"
                    onClick={() => {
                        setAccountEdit(true);
                        setPasswordEdit(false);
                    }}
                    style={{marginRight: "14px"}}
                >Edit profile</Button>
                <Button
                    type="primary"
                    onClick={() => {
                        setPasswordEdit(true);
                        setAccountEdit(false)
                    }}
                >Edit password</Button>
            </>
        )
    }
    return (
        <Skeleton loading={isLoading}>
            <Container>
                {body}
            </Container>
        </Skeleton>
    );
};

export default AccountDetail;
const Container = styled.div`
  .form-account-update, .form-password-edit {
    width: 70%;
  }
`