import React from 'react';
import styled from "styled-components";
import {emptyProductUrl} from "../../constants";

interface Props {
    id: number,
    name: string,
    image: string,
    price: number,
    quantity: number
}

const ProductInOrder = ({id, name, image, price, quantity}: Props) => {
    return (
        <Container>
            <div className="image-thumbnail">
                <img alt="product-image" className="product-image" src={image ?? emptyProductUrl}/>
            </div>
            <div className="detail">
                <div className="name">{name}</div>
                <div className="price">${price}</div>
            </div>
            <div className="quantity">
                Quantity: {quantity}
            </div>
        </Container>
    );
};

export default ProductInOrder;
const Container = styled.div`
  display: flex;
  align-items: center;
  height: 50px;
  width: 100%;
  margin: 4px 0;
  border-radius: 5px;
  background-color: var(--primaryColor);
  color: white;
  .image-thumbnail {
    .product-image {
      width: 50px;
      height: 50px;
      object-fit: cover;
    }
  }

  .detail {
    flex: 1 1 auto;
    margin-left: 10px;
    .name{
      width: 80px;
      height: 20px;
      font-weight: 500;
      font-size: 15px;
      word-wrap: break-word;
      white-space: normal;
      overflow: hidden;
      display: -webkit-box;
      text-overflow: ellipsis;
      -webkit-box-orient: vertical;
      -webkit-line-clamp: 2;
    }
  }
  .quantity{
    margin-right: 10px;
  }
`