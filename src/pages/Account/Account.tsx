import React from 'react';
import styled from "styled-components";
import {Tabs} from "antd";
import AccountDetail from "./AccountDetail";
import Order from "./Order";
import {InfoCircleOutlined, ShoppingCartOutlined} from "@ant-design/icons";

const {TabPane} = Tabs;
const Account = () => {
    return (
        <Container>
            <div className="content">
                <Tabs defaultActiveKey="1" tabPosition="left" style={{width: 1000, margin: "auto"}} size="large"
                      type="card">
                    <TabPane tab={<><InfoCircleOutlined/>Account details</>} key="1">
                        <AccountDetail/>
                    </TabPane>
                    <TabPane tab={<><ShoppingCartOutlined/>Orders</>} key="2">
                        <Order/>
                    </TabPane>
                </Tabs>
            </div>
        </Container>
    );
};

export default Account;
const Container = styled.div`
  min-height: calc(100vh - var(--headerHeight) - var(--footerHeight));
  display: flex;
  flex-direction: column;
  justify-content: center;
  .ant-tabs {
    padding: 20px;
    border: 1px solid var(--primaryColor);
    border-radius: 5px;
    box-shadow: var(--primaryBoxShadow);
  }
`