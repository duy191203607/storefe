import React from 'react';
import styled from "styled-components";
import {useGetAccountOrder} from "../../api/order";
import {Col, Collapse, Row, Skeleton, Typography} from "antd";
import {CaretRightOutlined} from '@ant-design/icons';
import OrderHeader from "./OrderHeader";
import ProductInOrder from "./ProductInOrder";

const {Title} = Typography;
const {Panel} = Collapse;
const Order = () => {
    const {data, isLoading} = useGetAccountOrder();
    console.log(data)
    return (
        <Skeleton loading={isLoading}>
            <Title level={4}>Account Orders</Title>
            <Container>
                <Collapse
                    bordered={false}
                    expandIcon={({isActive}) => <CaretRightOutlined rotate={isActive ? 90 : 0}/>}
                >
                    {
                        data?.map((v, i) => (
                            <Panel key={i} header={<OrderHeader id={v.id} createdAt={v.createdAt} price={v.price} address={v.address}/>}>
                                <Row className="product-item" gutter={[16, 16]}>
                                    {
                                        v.products.map((p, index) => (
                                            <Col span={8}>
                                                <ProductInOrder key={index} id={p.product.id} name={p.product.name}
                                                                price={p.product.price}
                                                                image={p.product.image} quantity={p.quantity}/>
                                            </Col>
                                        ))
                                    }
                                </Row>
                            </Panel>
                        ))
                    }
                </Collapse>
            </Container>
        </Skeleton>
    );
};

export default Order;
const Container = styled.div`
  max-height: 550px;
  overflow-y: auto;
  [data-theme='compact'] .site-collapse-custom-collapse .site-collapse-custom-panel,
  .site-collapse-custom-collapse .site-collapse-custom-panel {
    margin-bottom: 24px;
    overflow: hidden;
    background: #f7f7f7;
    border: 0;
    border-radius: 2px;
  }
`