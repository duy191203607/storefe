import React from 'react';
import styled from "styled-components";
import * as Yup from 'yup';
import {Button, message, Typography} from "antd";
import {Link, useNavigate} from "react-router-dom";
import InputField from "../../components/common/InputField";
import {Formik, Form, FormikValues} from "formik";
import {useAuthStore} from "../../store/auth";
import {useLogin} from "../../api/auth";

const {Title} = Typography;
const LoginSchema = Yup.object().shape({
    email: Yup.string().email('Invalid email').required('This field is require'),
    password: Yup.string().required('This field is require'),
});
const Login = () => {
    const _Login = useLogin();
    const navigate = useNavigate();
    const {updateAuthState} = useAuthStore();
    const onSubmit = async (values: FormikValues) => {
        await _Login.mutateAsync({email: values.email, password: values.password}, {
            onSuccess: (res: any) => {
                const {id, name, email} = res.data;
                updateAuthState({userID: id, name, email, isAuth: true});
                message.success(res?.message, 3);
                navigate("/", {replace: true})
            },
            onError: (error: any) => {
                message.error(error.response.data.error, 3);
            }
        })
    }
    return (
        <Container>
            <Title level={3} className="title">Login</Title>
            <Formik
                initialValues={{
                    email: '',
                    password: '',
                }}
                validationSchema={LoginSchema}
                onSubmit={onSubmit}
            >
                {({values, errors, touched}) => (
                    <Form autoComplete="off">
                        <InputField errors={errors} touched={touched} values={values} name="email" placeholder="Email"/>
                        <InputField errors={errors} touched={touched} values={values} name="password" type="password"
                                    placeholder="Password"/>
                        <div>
                            <Link to="/auth/register">
                                Don't have an account? Register now
                            </Link>
                        </div>
                        <div className="submit-button" style={{marginTop: "10px"}}>
                            <Button loading={_Login.isLoading} htmlType="submit" type="primary"
                                    style={{width: "100%"}}>Login</Button>
                        </div>
                    </Form>
                )}
            </Formik>
        </Container>
    );
};
export default Login;
const Container = styled.div`
  width: 400px;
  margin: auto;
  height: calc(100vh - var(--headerHeight) - var(--footerHeight));
  display: flex;
  flex-direction: column;
  justify-content: center;
  .title {
    text-align: center;
  }
`