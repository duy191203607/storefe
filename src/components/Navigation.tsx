import React, {ChangeEvent, useState} from 'react';
import {Button, Dropdown, Input, Menu, message, Layout} from "antd";
// import {Header} from "antd/es/layout/layout";
import {appRoute} from "../routes";
import {NavLink, useLocation, useNavigate} from "react-router-dom";
import styled from "styled-components";
import {CloseCircleOutlined, SearchOutlined, ShoppingCartOutlined} from '@ant-design/icons';
import {useCartStore} from "../store/cart";
import {useAuthStore} from "../store/auth";
import {DownOutlined} from '@ant-design/icons';
import {useLogout} from "../api/auth";
import {useGetProductByName} from "../api/product";
import {throttle} from "lodash"
import Loading from "./common/Loading";
import SearchItem from "./common/SearchItem";
import useOnClickOutside from "../hooks/useComponentVisible";
const {Header} = Layout
const Navigation = () => {
    const {toggleCart, totalItem, clearCart} = useCartStore();
    const {isAuth, name, logout} = useAuthStore();
    const _Logout = useLogout();
    const location = useLocation();
    const navigation = useNavigate();
    const [showSearchResult, setShowSearchResult] = useState(false);
    const [searchValue, setSearchValue] = useState<string>("")
    const {data: ProductSearchResult, isLoading} = useGetProductByName(searchValue);
    const ref = React.useRef() as React.MutableRefObject<HTMLInputElement>;
    useOnClickOutside(ref, () => setShowSearchResult(false));
    const matchActiveRoute = () => {
        const result = [];
        const idx = appRoute.publicRoute.findIndex(v => v.path === location.pathname);
        result.push(idx.toString());
        return result;
    }
    const logoutUser = async () => {
        await _Logout.mutateAsync(undefined, {
            onSuccess: async (data) => {
                logout();
                clearCart();
                message.success("Logout success", 3);
            },
            onError: (error: any) => {
                console.log(error)
            }
        })
    }
    const userMenu = (
        <Menu theme="light">
            <Menu.Item key="0" onClick={() => navigation("/account")}>
                Account details
            </Menu.Item>
            <Menu.Item key="1" onClick={logoutUser}>
                <span>Logout</span>
            </Menu.Item>
        </Menu>
    )
    const onSearchProductChange = throttle((e: ChangeEvent<HTMLInputElement>) => {
        setShowSearchResult(true);
        setSearchValue(e.target.value)
    }, 700, {leading: false})
    return (
        <AppHeader>
            <Menu theme="light" mode="horizontal" selectedKeys={matchActiveRoute()}
                  defaultSelectedKeys={matchActiveRoute()} className="main-menu">
                {
                    appRoute.publicRoute.filter((v) => v.display).map((v, i) => (
                        <Menu.Item key={i}>
                            <NavLink to={v.path} className={({isActive}) => (isActive ? 'active' : 'inactive')}>
                                {v.name}
                            </NavLink>
                        </Menu.Item>
                    ))
                }
            </Menu>

            <div className="nav-right">
                <div className="shop-cart-icon" style={{cursor: "pointer"}}>
                    <ShoppingCartOutlined className="shop-cart" style={{width: "40%"}}/>
                    <div onClick={toggleCart}>Cart ({totalItem})</div>
                </div>
                <div className="product-search">
                    <Input
                        className="product-search"
                        placeholder="Tìm kiếm sản phẩm"
                        size="large"
                        style={{width: "100%"}}
                        suffix={<SearchOutlined/>}
                        onChange={(e: ChangeEvent<HTMLInputElement>) => onSearchProductChange(e)}
                        onFocus={() => setShowSearchResult(true)}
                        // value={searchValue}
                        defaultValue={searchValue}
                    />
                    {
                        showSearchResult &&
                        <div className="search-result" ref={ref}>
                            {
                                isLoading ? <Loading size={100}/> : (
                                    <>
                                        {
                                            (ProductSearchResult?.length === 0 || !ProductSearchResult) ? (
                                                <div style={{textAlign: "center", margin: "auto"}}>Nothing to show</div>
                                            ) : (
                                                <>
                                                    {
                                                        ProductSearchResult?.map((v, i) => (
                                                            <SearchItem toggleSearch={() => setShowSearchResult(false)}
                                                                        key={i} id={v.id} name={v.name} price={v.price}
                                                                        image={v.image as string}/>
                                                        ))
                                                    }
                                                </>
                                            )
                                        }
                                    </>
                                )
                            }
                        </div>
                    }
                </div>
                {
                    isAuth ? (
                        <div className="user-account-action">
                            <Dropdown overlay={userMenu} trigger={["click"]}>
                                <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                                    {name ?? "User"} <DownOutlined/>
                                </a>
                            </Dropdown>
                        </div>
                    ) : (
                        <div className="auth-button">
                            {
                                appRoute.authRoute.map((v, i) => (
                                    <NavLink to={v.path} className={({isActive}) => (isActive ? 'active' : 'inactive')}
                                             key={i}>
                                        <Button type="primary" size="large" style={{width: "100px", margin: "0 4px"}}>
                                            {v.name}
                                        </Button>
                                    </NavLink>
                                ))
                            }
                        </div>
                    )
                }
            </div>
        </AppHeader>
    );
};

export default Navigation;
const AppHeader = styled(Header)`
  font-weight: 500;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: white;
  .ant-menu-item:nth-child(5) {
    margin-left: auto;
  }
  .main-menu{
    width: 600px;
  }
  .nav-right {
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 600px;

    .shop-cart-icon {
      //width: 20%;
      display: flex;
      align-items: center;
      //.shop-cart {
      //  display: flex;
      //  align-items: center;
      svg {
        width: 50%;
        height: 50%;
        font-size: 50px
      }
    }
  }

  .product-search {
    //width: 40%;
    position: relative;
    flex: 0.6 1 auto;

    .search-result {
      height: 250px;
      width: 100%;
      position: absolute;
      background-color: white;
      z-index: 100;
      border: 1px solid var(--primaryColor);
      overflow-y: auto;
      

      .close-search {
        cursor: pointer;
        width: 100%;
        height: 20px;
        display: flex;
        justify-content: center;
        align-items: center;
        background-color: var(--primaryColor);
        color: white;
        position: sticky;
      }
    }
  }

  .auth-button {
  }

  .user-account-action {
    //width: 40%;
  }
`