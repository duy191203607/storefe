import React from 'react';
import styled from "styled-components";
import {Button, Image, Row, Col} from "antd";
import {PlusCircleOutlined, MinusCircleOutlined, DeleteOutlined} from "@ant-design/icons";
import {useCartStore} from '../store/cart';

interface Props {
    image: string;
    name: string;
    id: number;
    price: number;
    quantity: number;
}

const CartItem = ({image, name, price, quantity, id}: Props) => {
    const {incrementProduct, decrementProduct, removeItem} = useCartStore();
    return (
        <Container>
            <Col className='product-image' span={4}>
                <Image preview={false} src={image}/>
            </Col>
            <Col className="product-info" span={10}>
                <div className="product-name">
                    {name}
                </div>
                <div className="product-price">
                    ${price}
                </div>
            </Col>
            <Col span={5} className="product-quantity">
                <span>Quantity: {quantity}</span>
            </Col>
            <Col className="product-cart-action" span={5}>
                <Button
                    type="primary"
                    icon={<PlusCircleOutlined/>}
                    style={{marginRight: "10px", width: "50px"}}
                    size="large"
                    onClick={() => incrementProduct(id)}
                />
                {
                    quantity === 1 ?
                        <Button
                            type="default"
                            icon={<DeleteOutlined/>}
                            size="large"
                            style={{width: "50px"}}
                            onClick={() => removeItem(id)}
                        /> :
                        <Button
                            type="default"
                            icon={<MinusCircleOutlined/>}
                            size="large"
                            onClick={() => decrementProduct(id)}
                            style={{width: "50px"}}
                        />
                }
            </Col>
        </Container>
    );
};

export default CartItem;
const Container = styled(Row)`
  margin-bottom: 10px;

  .product-image {
    .ant-image-img {
      width: 70px;
      height: 100%;
      object-fit: cover;
    }
  }

  .product-info {
    display: flex;
    flex-direction: column;
    justify-content: space-around;

    .product-price {
      color: #8d8d8d;;
    }

    .product-name {
      font-weight: bold;
    }
  }

  .product-quantity {
    display: flex;
    align-items: center;
    color: #8d8d8d;;
  }

  .product-cart-action {
    display: flex;
    align-items: center;
  }
`