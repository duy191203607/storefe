import React from 'react';
import {Card, Image} from "antd";
import styled, {keyframes} from "styled-components";
import {PlusCircleOutlined, ShoppingCartOutlined} from "@ant-design/icons";
import {useCartStore} from "../../store/cart";
import {Link} from "react-router-dom";

interface Props {
    id: number;
    name: string,
    price: number,
    image: string,
    isLoading: boolean
}

const ProductItem = ({id, name, price, isLoading, image}: Props) => {
    const {addProduct, incrementProduct, checkItemInCart} = useCartStore();
    const addToCart = () => {
        addProduct({id, price, name, image});
    }
    return (
        <ProductItemContainer>
            <Link to={`/product/${id}`}>
                <Card
                    loading={isLoading}
                    hoverable
                    cover={<Image src={image} preview={false} loading="eager"/>}
                    style={{height: '100%'}}
                >
                    <div className="product-info">
                        <div className="name">{name}</div>
                        <div className="price">${price}</div>
                    </div>
                </Card>
            </Link>
            {
                !checkItemInCart(id) ? (
                    <AddToCartHover className="add-to-cart" onClick={addToCart}>
                        Add to cart <ShoppingCartOutlined/>
                    </AddToCartHover>
                ) : (
                    <AddToCartHover className="add-to-cart" onClick={() => incrementProduct(id)}>
                        Add more <PlusCircleOutlined/>
                    </AddToCartHover>
                )
            }

        </ProductItemContainer>
    );
};

export default ProductItem;
const addToCartDisplay = keyframes`
  from {
    opacity: 0;
    transform: translateX(-50%);
  }
  to {
    opacity: 1;
    transform: translateX(0%);
  }
`
const ProductItemContainer = styled.div`
  position: relative;
  width: 100%;
  height: 300px;

  .ant-card-cover {
    width: 100%;
    height: 150px;
    margin: 0;
  }

  .ant-image {
    width: 100%;
    height: 100%;
    display: inline-block;
    overflow: hidden;

    img {
      display: block;
      width: 100%;
      height: 100%;
      object-fit: cover;
      -moz-transition: all 0.3s;
      -webkit-transition: all 0.3s;
      transition: all 0.3s;
    }
  }

  .product-info {
    display: flex;
    justify-content: space-between;
    font-weight: bold;
  }

  &:hover > .add-to-cart {
    display: flex;
    justify-content: center;
    align-items: center;
  }
  &:hover .ant-image img{
    transform: scale(1.04);
  }
`
const AddToCartHover = styled.div`
  background-color: var(--primaryColor);
  color: white;
  font-weight: bold;
  display: none;
  height: 50px;
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  animation: ${addToCartDisplay} 0.2s ease-in;
  cursor: pointer;

  svg {
    font-size: 20px;
    margin-left: 10px;
  }
`
const Price = styled.div``