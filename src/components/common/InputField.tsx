import React from 'react';
import styled from "styled-components";
import {Field} from "formik";


const InputField = ({errors, touched, values, ...props}: any) => {
    return (
        <Container>
            <div className="text-input">
                {props.label && <label htmlFor={props.name}>{props.label}</label>}
                <Field {...props} type={props.type ?? "text"}
                       value={values[props.name]}
                       className={(errors && errors[props.name]) ? "input-field input-status-error" : "input-field"}
                       placeholder={props.placeholder}/>
            </div>
            {(errors && errors[props.name] && touched[props.name]) ? (
                <InputError className="input-error-message">{errors[props.name]}</InputError>) : null}
        </Container>
    );
};

export default InputField;
const InputError = styled.div`
  color: red;
`
const Container = styled.div`
  width: 100%;
  margin: 8px 0;
  display: block;
  border-radius: 4px;
  box-sizing: border-box;
  transition: 0.3s all;

  .text-input {
    display: flex;
    flex-direction: column;
  }

  input[type=text], input[type=password] {
    height: 35px;
    -webkit-transition: all .4s ease-out;
    -moz-transition: all .4s ease-out;
    -ms-transition: all .4s ease-out;
    -o-transition: all .4s ease-out;
    transition: all .4s ease-out;
    border-radius: 4px;
    padding-left: 10px;
    border: 1px solid var(--primaryColor);
  }

  .input-field {
    outline: none;
  }

  .input-field:focus, .input-field:active {
    box-shadow: rgba(99, 99, 99, 0.2) 0 2px 8px 0px;
  }

  .input-status-error, .input-status-error:focus {
    border: 1px solid red;
    outline: none;
  }
`