import React from 'react';
import {IProduct} from "../../store/cart";
import styled from "styled-components";
import {Image} from "antd";
import {useNavigate} from "react-router-dom";

interface Props {
    toggleSearch: () => void
}

const SearchItem = ({name, price, image, id, toggleSearch}: IProduct & Props) => {
    const navigation = useNavigate();
    const onClick = () => {
        navigation(`/product/${id}`);
        toggleSearch();
    }
    return (
        <Container onClick={onClick}>
            <div className="product-image">
                <Image src={image} preview={false}/>
            </div>
            <div className="product-info">
                <div className="product-name">{name}</div>
                <div className="product-price"> Price: {price}</div>
            </div>
        </Container>
    )
};

export default SearchItem;
const Container = styled.div`
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  height: 60px;
  padding: 6px;
  margin: 4px 0;
  transition: all 0.2s ease-in-out;

  &:hover {
    background-color: var(--primaryColor);
    color: white;
  }

  .product-image {
    width: 60px;
    height: 60px;

    .ant-image {
      height: 100%;
      display: flex;
      align-items: center;

      img {
        width: 100%;
        height: 60px;
        object-fit: cover;
      }
    }

  }

  .product-info {
    margin-left: 10px;
    display: flex;
    flex-direction: row;
    flex: 1 1 auto;
    font-size: 12px;

    .product-price {
      margin-left: 10px;
    }
  }
`