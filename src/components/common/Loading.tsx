import React from 'react';
import {LoadingOutlined} from "@ant-design/icons";
import {Spin} from "antd";
import styled from "styled-components";

interface Props {
    size?: number
}

const Loading = ({size}: Props) => {
    const antIcon = <LoadingOutlined style={{fontSize: !size ? 200 : size}} spin/>;
    return (
        <Container>
            <Spin indicator={antIcon}/>
        </Container>
    );
};

export default Loading;
const Container = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: auto;
`;