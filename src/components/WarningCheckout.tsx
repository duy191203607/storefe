import React from 'react';
import {Button, Modal, Result} from "antd";
import {Link, useNavigate} from "react-router-dom";
import {useCartStore} from "../store/cart";

interface Props {
    showModal: boolean,
    toggleModal: () => void
}

const WarningCheckout = ({showModal, toggleModal}: Props) => {
    const navigation = useNavigate();
    const {toggleCart} = useCartStore()
    const content = (
        <>
            <Result
                status="warning"
                title="Please login before checkout!"
                extra={[
                    <>
                        <Button type="primary" onClick={() => {
                            navigation("/auth/login");
                            toggleCart();
                            toggleModal();
                        }}>
                            Go to login
                        </Button>
                        <Button onClick={toggleModal}>Cancel</Button>
                    </>
                ]}
            />
        </>
    )
    return (
        <Modal children={content} visible={showModal} onCancel={toggleModal} footer={null}/>
    );
};

export default WarningCheckout;