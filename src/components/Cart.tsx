import React, {useState} from 'react';
import CartItem from "./CartItem";
import {Button, Col, Divider, Drawer, message, Row, Typography} from "antd";
import styled from "styled-components";
import {useCartStore} from "../store/cart";
import WarningCheckout from "./WarningCheckout";
import {useAuthStore} from "../store/auth";
import {useNavigate} from "react-router-dom";

const {Title} = Typography;
const Cart = ({visible, onClose}: any) => {
    const {products, totalItem, totalPrice, clearCart, toggleCart} = useCartStore();
    const {isAuth} = useAuthStore();
    const [showModal, setShowModal] = useState(false);
    const navigation = useNavigate();
    const handleCheckout = async () => {
        if (!isAuth) {
            if (totalItem === 0) {
                message.error("Cart empty for checkout!", 3)
            } else {
                setShowModal(!showModal);
            }
        } else {
            if (totalItem === 0) {
                message.error("Cart empty for checkout!", 3)
            } else {
                navigation("/checkout");
                toggleCart()
            }
        }
    }
    return (
        <Container visible={visible} onClose={onClose} size={"large"}>
            <div className="product-item">
                {
                    products.length >= 1 ? products.map((v, i) => (
                        <CartItem key={i} id={v.id} name={v.name} price={v.price} image={v.image}
                                  quantity={v.quantity}/>
                    )) : <div style={{textAlign: "center", fontWeight: "bold"}}>Empty</div>
                }
            </div>
            <Divider/>
            <Row>
                {
                    products.length >= 1 && (
                        <CartCalculation span={12} offset={12} className="cart-calculation">
                            <p>Total item: {totalItem}</p>
                            <p>Total price: ${totalPrice}</p>
                            <Button
                                type={"primary"}
                                style={{width: "100px", marginRight: "10px"}}
                                size="large"
                                onClick={handleCheckout}
                            >
                                Checkout
                            </Button>
                            <Button type="default" style={{width: "100px", marginTop: "14px"}} size="large"
                                    onClick={clearCart}>Clear</Button>
                        </CartCalculation>
                    )
                }
            </Row>
            <WarningCheckout showModal={showModal} toggleModal={() => setShowModal(!showModal)}/>
        </Container>
    );
};


export default Cart;
const Container = styled(Drawer)`
  .product-item {
    //max-height: 650px;
    overflow-y: auto;
  }`
const CartCalculation = styled(Col)`
  //text-align: right;
  padding-right: 20px;

  p {
    font-weight: bold;
    margin: 6px 0;
    font-size: 15px;
  }
`;