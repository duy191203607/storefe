import React from 'react';
import {Navigate} from "react-router-dom";
import {useAuthStore} from "../store/auth";

interface Props {
    children: React.LazyExoticComponent<() => JSX.Element> | JSX.Element
}

const PrivateRoute = ({children}: Props) => {
    const {isAuth} = useAuthStore();
    return isAuth ? children : <Navigate to="/auth/login"/>
};

export default PrivateRoute;