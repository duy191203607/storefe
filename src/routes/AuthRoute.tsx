import React from 'react';
import {useAuthStore} from "../store/auth";
import {Navigate} from "react-router-dom";

interface Props {
    children: React.LazyExoticComponent<() => JSX.Element> | JSX.Element
}

const AuthRoute = ({children}: Props) => {
    const {isAuth} = useAuthStore();
    return !isAuth ? children : <Navigate to="/"/>
};

export default AuthRoute;