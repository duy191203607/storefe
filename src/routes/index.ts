import React, {lazy} from "react";

const Login = lazy(() => import("../pages/Login/Login"));
const Register = lazy(() => import("../pages/Register/Register"));
const Home = lazy(() => import("../pages/Home/Home"));
const Shop = lazy(() => import("../pages/Shop/Shop"));
const ProductDetail = lazy(() => import("../pages/ProductDetail/ProductDetail"));
const Checkout = lazy(() => import("../pages/Checkout/Checkout"));
const Account = lazy(() => import("../pages/Account/Account"));
const AboutUs = lazy(() => import("../pages/AboutUs/AboutUs"));

interface IRoute {
    name: string,
    path: string,
    display?: boolean,
    component?: React.LazyExoticComponent<() => JSX.Element>
}

interface IAppRoute {
    publicRoute: IRoute[],
    privateRoute: IRoute[],
    authRoute: IRoute[]
}

export const appRoute: IAppRoute = {
    publicRoute: [
        {
            name: "Home",
            path: "/",
            component: Home,
            display: true
        },
        {
            name: "Shop",
            path: "/shop",
            component: Shop,
            display: true,
        },
        {
            name: "Product Detail",
            path: "/product/:id",
            component: ProductDetail,
            display: false
        },
        // {
        //     name: "About Us",
        //     path: "/about-us",
        //     component: AboutUs,
        //     display: true
        // },
        // {
        //     name: "Recommend",
        //     path: "/recommended"
        // },
    ],
    privateRoute: [
        {
            name: "Checkout",
            path: "/checkout",
            component: Checkout,
        },
        {
            name: "Account",
            path: "/account",
            component: Account
        }
    ],
    authRoute: [
        {
            name: "Login",
            path: "/auth/login",
            component: Login
        },
        {
            name: "Register",
            path: "/auth/register",
            component: Register
        }
    ]
}