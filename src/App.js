import {Drawer, Layout} from "antd";
import Navigation from "./components/Navigation";
import styled from "styled-components";
import {appRoute} from "./routes";
import {Routes, Route} from "react-router-dom";
import {v4 as uuidv4} from 'uuid';
import React, {Suspense} from 'react';
import PageNotFound from "./components/PageNotFound";
import {useCartStore} from "./store/cart";
import AuthRoute from "./routes/AuthRoute";
import Loading from "./components/common/Loading";
import Cart from "./components/Cart";
import PrivateRoute from "./routes/PrivateRoute";

const {Content, Footer,} = Layout;


function App() {
    const {isShow, toggleCart} = useCartStore()
    return (
        <Layout>
            <Navigation/>
            <Cart visible={isShow} onClose={toggleCart}/>
            <Content style={{padding: '0 50px'}}>
                <Suspense fallback={<Loading/>}>
                    <Routes>
                        {
                            appRoute.publicRoute.map((r, i) => (
                                <Route index={i === 0} key={uuidv4()} path={r.path} element={
                                    <r.component/>
                                }/>
                            ))
                        }
                        {
                            appRoute.privateRoute.map((r, i) => {
                                return (
                                    <Route
                                        path={r.path}
                                        key={uuidv4()}
                                        element={
                                            <PrivateRoute>
                                                <r.component/>
                                            </PrivateRoute>
                                        }
                                    />
                                )
                            })
                        }
                        {
                            appRoute.authRoute.map((r, i) => {
                                return (
                                    <Route
                                        path={r.path}
                                        key={uuidv4()}
                                        element={
                                            <AuthRoute>
                                                <r.component/>
                                            </AuthRoute>
                                        }
                                    />
                                )
                            })
                        }
                        <Route path="*" element={<PageNotFound/>}/>
                    </Routes>
                </Suspense>
            </Content>
            <AppFooter style={{textAlign: 'center'}}>
                <AppFooterContainer>
                    <span>CHÍNH SÁCH BẢO MẬT </span>
                    <span>QUY CHẾ HOẠT ĐỘNG</span>
                    <span>CHÍNH SÁCH VẬN CHUYỂN</span>
                    <span>CHÍNH SÁCH TRẢ HÀNG VÀ HOÀN TIỀN</span>
                    <span onClick={() => window.open("http://127.0.0.1:5500/index.html", "_blank")}>VỀ CHÚNG TÔI</span>
                </AppFooterContainer>
            </AppFooter>
        </Layout>
    )
}

export default App;
const AppFooter = styled(Footer)`
  //margin-top: 10px;
  background-color: var(--primaryColor);
  color: white;
`
const AppFooterContainer = styled.div`
  width: 70%;
  display: flex;
  justify-content: space-between;
  margin: auto;

  span, a {
    cursor: pointer;
    color: whitesmoke;
  }
`
