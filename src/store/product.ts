import create from "zustand";

export interface IPagination {
    page: number,
    pageSize?: number,
    category?: number | null,
    sort?: Sort,
}

export type Sort = "ASC" | "DESC" | null;

interface IPaginationFunc {
    updateSort: (value: Sort) => void,
    updateCategory: (value: number) => void,
    updatePaginationPage: (page: number, pageSize: number) => void,
    resetPagination: () => void
}

export const useShopProductPagination = create<IPagination & IPaginationFunc>((set, get) => ({
    page: 1,
    category: null,
    sort: null,
    pageSize: undefined,
    updateSort: (value: Sort) => set({sort: value}),
    updateCategory: (value) => set({category: value}),
    updatePaginationPage: (page, pageSize) => set({page: page, pageSize: pageSize}),
    resetPagination: () => set({page: 1}, false)
}))