import create from "zustand";
import {persist} from "zustand/middleware";

export interface IProduct {
    id: number;
    name: string,
    price: number,
    image: string,
}

interface INewProduct extends IProduct {
    quantity: number;
}

export interface ICart {
    isShow: boolean,
    cartSubmitting?: boolean,
    products: INewProduct[],
    totalItem: number,
    totalPrice: number,
    toggleCart: () => void,
    addProduct: (product: IProduct) => void,
    removeItem: (id: number) => void,
    incrementProduct: (id: number) => void,
    decrementProduct: (id: number) => void,
    checkItemInCart: (id: number) => boolean,
    clearCart: () => void,
    toggleCartSubmit: () => void,
}

const totalItemInCart = (products: INewProduct[]) => {
    const itemCount = products.reduce((total, product) => {
        return total + product.quantity;
    }, 0);
    const totalPrice = products.reduce((total, product) => total + product.price * product.quantity, 0).toFixed(2);
    return {itemCount, totalPrice};
}
export const useCartStore = create(persist<ICart>(
    (set, get) => ({
        isShow: false,
        products: [],
        totalItem: 0,
        totalPrice: 0,
        cartSubmitting: false,
        addProduct: async (product: IProduct) => {
            const newProduct: INewProduct = {
                ...product,
                quantity: 1,
            };
            set({products: [...get().products, newProduct]});
            const {totalPrice, itemCount} = totalItemInCart(get().products);
            set({totalPrice: parseInt(totalPrice), totalItem: itemCount});
        },
        toggleCart: () => set({isShow: !get().isShow}),
        removeItem: (id) => {
            const newProducts = get().products.filter(product => product.id !== id);
            set({products: newProducts});
            const {totalPrice, itemCount} = totalItemInCart(get().products);
            set({totalPrice: parseInt(totalPrice), totalItem: itemCount});
        },
        incrementProduct: (id) => {
            get().products[get().products.findIndex(product => product.id === id)].quantity += 1;
            const {totalPrice, itemCount} = totalItemInCart(get().products);
            set({totalPrice: parseInt(totalPrice), totalItem: itemCount});
        },
        decrementProduct: (id) => {
            get().products[get().products.findIndex(product => product.id === id)].quantity -= 1;
            const {totalPrice, itemCount} = totalItemInCart(get().products);
            set({totalPrice: parseInt(totalPrice), totalItem: itemCount});
        },
        checkItemInCart: (id) => {
            const idx = get().products.findIndex(product => product.id === id);
            return idx > -1
        },
        clearCart: () => set({products: [], totalItem: 0, totalPrice: 0}),
        toggleCartSubmit: () => set({cartSubmitting: !get().cartSubmitting})
    }),
    {
        name: "user-cart",
        getStorage: () => localStorage
    }
));