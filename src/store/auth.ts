import create from "zustand";
import {persist} from "zustand/middleware"
import {useCartStore} from "./cart";

export interface IAuth {
    userID: number | null,
    isAuth: boolean,
    name: string | null,
    email: string | null,
    updateAuthState: (v: UpdateAuthState) => void,
    logout: () => void
}

interface UpdateAuthState {
    userID: number,
    isAuth: boolean,
    name: string,
    email: string,
}
export const useAuthStore = create(persist<IAuth>(
    set => ({
        isAuth: false,
        userID: null,
        name: null,
        email: null,
        updateAuthState: (v: UpdateAuthState) => {
            set({isAuth: v.isAuth, name: v.name, userID: v.userID, email: v.email})
        },
        logout: () => {
            set({isAuth: false, userID: null, name: null, email: null})
        }
    }),
    {
        name: "user-info",
        getStorage: () => localStorage
    }
))
