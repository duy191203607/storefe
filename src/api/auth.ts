import {axiosInstance} from "../config/axios";
import {useMutation} from "react-query";

export interface ILogin {
    email: string,
    password: string
}

export interface IRegister {
    name: string,
    email: string,
    password: string,
    phone?: string,
    address?: string
}

const login = async (loginData: ILogin) => {
    try {
        const {email, password} = loginData;
        return await axiosInstance.post("/auth/login", {
            email,
            password
        });
    } catch (e: any) {
        throw e
    }
}
const logout = async () => {
    try {
        return await axiosInstance.post("/auth/logout")
    } catch (e) {
        throw e;
    }
}
const register = async ({name, email, password}: IRegister) => {
    try {
        return await axiosInstance.post("/auth/register", {
            name,
            email,
            password
        });
    } catch (e) {
        throw e;
    }
}
export const useLogin = () => {
    return useMutation(['auth/login'], (loginData: ILogin) => login(loginData))
}
export const useLogout = () => {
    return useMutation("auth/logout", logout);
}
export const useRegister = () => {
    return useMutation(["auth/register"], (registerData: IRegister) => register(registerData))
}