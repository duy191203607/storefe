import {useMutation, useQuery} from "react-query";
import {axiosInstance} from "../config/axios";

interface Profile {
    address: string | null,
    image: string | null,
    name: string,
    phone: string | null
}

interface IAccountInfo {
    email: string,
    id: number,
    profile: Profile
}

interface IUpdateAccountInfo {
    name?: string,
    email: string,
    phone?: string,
    address?: string,
}

interface IUpdateAccountPassword {
    newPassword: string
}

const getAccountDetail = async (): Promise<IAccountInfo> => {
    try {
        const {data} = await axiosInstance.get("/account/info");
        return data
    } catch (e) {
        throw e;
    }
}
const updateAccountInfo = async ({name, email, phone, address}: IUpdateAccountInfo) => {
    try {
        const {data} = await axiosInstance.post("/account/update/info", {
            email,
            ...(name && {name}),
            ...(phone && {phone}),
            ...(address && {address})
        })
        return data
    } catch (e) {
        throw e;
    }
}
const updateAccountPassword = async ({newPassword}: IUpdateAccountPassword) => {
    try {
        await axiosInstance.post("/account/update/password", {
            newPassword
        })
    } catch (e) {
        throw e;
    }
}
export const testCurrentPassword = async (currentPassword: string, accountId: number) => {
    try {
        const {data} = await axiosInstance.post("/auth/test-current-pass", {
            currentPassword,
            accountId
        });
        return data;
    } catch (e) {
        throw e;
    }
}
export const useGetAccountDetail = () => {
    return useQuery(['account/detail'], () => getAccountDetail())
}
export const useUpdateAccountInfo = () => {
    return useMutation(["account/update/info"], (newInfo: IUpdateAccountInfo) => updateAccountInfo(newInfo))
}
export const useUpdateAccountPassword = () => {
    return useMutation(["account/update/password"], (newPassword: IUpdateAccountPassword) => updateAccountPassword(newPassword))
}