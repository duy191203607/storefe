import {useQuery} from "react-query";
import {axiosInstance} from "../config/axios";
interface CategoryList {
    id: number,
    name: string
}
const getCategory = async (): Promise<CategoryList[]> => {
    try {
        const {data} = await axiosInstance.get("/category");
        return data;
    } catch (e) {
        throw e;
    }
}
export const useGetCategory = () => {
    return useQuery(["shop/getProduct"], getCategory)
}