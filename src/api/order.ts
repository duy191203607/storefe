import {axiosInstance} from "../config/axios";
import {useMutation, useQuery} from "react-query";
import {IProduct} from "../store/cart";

interface INewProductInOrder {
    id: number,
    quantity: number
}

export interface ICreateNewOrder {
    accountId: number,
    product: INewProductInOrder[],
    price: number,
    name: string,
    address: string,
    email?: string,
    phone: number,
}

interface IAccountOrderProduct {
    id: string,
    quantity: number
    product: IProduct
}

export interface IAccountOrder {
    id: number,
    price: number,
    name: string,
    phone: string,
    email?: string,
    address: string,
    createdAt: string,
    products: IAccountOrderProduct[]
}

const createNewOrder = async ({accountId, product, price, name, address, email, phone}: ICreateNewOrder) => {
    try {
        return await axiosInstance.post("/order/create", {
            accountId,
            product,
            price,
            name,
            address,
            phone,
            ...(email && {email})
        })
    } catch (e) {
        throw e;
    }
}
export const useCreateNewOrder = () => {
    return useMutation(['shop/order/create'], (newOrder: ICreateNewOrder) => createNewOrder(newOrder))
}
const getAccountOrder = async (): Promise<IAccountOrder[]> => {
    try {
        const {data} =  await axiosInstance.get("/account/order");
        return data;
    } catch (e) {
        throw e;
    }
}
export const useGetAccountOrder = () => {
    return useQuery(['account/get/order'], getAccountOrder)
}

