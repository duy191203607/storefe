import {useQuery} from "react-query";
import {axiosInstance} from "../config/axios";
import {IPagination, useShopProductPagination} from "../store/product";

interface IProduct {
    id: number,
    name: string,
    image: string | null,
    inventory: number,
    price: number,
    createdAt?: string,
    updatedAt?: string
}

interface IProductResult {
    total: number,
    result: IProduct[]
}

interface Category {
    id: number,
    name: string,
}

interface IProductDetail extends IProduct {
    category: Category
}

const getProduct = async ({page, category, sort, pageSize}: IPagination): Promise<IProductResult> => {
    try {
        const {data} = await axiosInstance.get("/product/query", {
            params: {
                page,
                // @ts-ignore
                ...((category !== "null" && category) && {category: category}),
                // @ts-ignore
                ...((sort !== "null") && {sort: sort}),
                ...(pageSize && {pageSize: pageSize})
            }
        });
        return data
    } catch (e) {
        throw e;
    }
}
const findById = async (id: number): Promise<IProductDetail> => {
    try {
        const {data} = await axiosInstance.get(`/product/${id}`);
        return data;
    } catch (e) {
        throw e;
    }
}
const searchByName = async (name: string): Promise<IProduct[]> => {
    try {
        const {data} = await axiosInstance.get(`/product/search`, {
            params: {
                ...((name !== "") && {name: name})
            }
        });
        return data;
    } catch (e) {
        throw e;
    }
}
export const useGetProduct = () => {
    const {page, category, sort, pageSize} = useShopProductPagination()
    return useQuery(["shop/getProduct", page, category, sort, pageSize], () => getProduct({
        page,
        category,
        sort,
        pageSize
    }))
}
export const useGetProductById = (id: number) => {
    return useQuery(["shop/product/byId", id], () => findById(id), {enabled: !!id});
}
export const useGetProductByName = (name: string) => {
    return useQuery(["shop/searchProduct/byName", name], () => searchByName(name), {
        enabled: (name !== "")
    });
}